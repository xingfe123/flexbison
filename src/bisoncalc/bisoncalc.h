extern int yylineno;
void yyerror(const char* s,...);
struct ast{
  int nodetype;
  struct ast *left;
  struct ast *right;
};
struct numval{
  int nodetype;
  double number;
};
struct ast* newast(int nodetype,struct ast* left,  struct ast * right);
struct ast* newnum(const double d);
double eval(const struct ast *);
void treefree(struct ast *);


  
