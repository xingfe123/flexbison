#include<stdio.h>
#include<stdlib.h>
#include<stdarg.h>
#include<strings.h>
#include "bisoncalc.h"
struct ast* newast(int nodetype,   struct ast * left,  struct ast* right){
   struct ast *a = malloc(sizeof(struct ast));
  if(!a){
    yyerror("out of space");
    exit(0);
  }
  bzero(a,sizeof(struct ast));
  a->nodetype = nodetype;
  a->right = right;
  a->left = left;
  return a;
}
struct ast* newnum(const double d){
  struct numval *a = malloc(sizeof(struct numval));
  if(!a){
    yyerror("out of space");
    exit(0);
  }
  bzero(a,sizeof(struct ast));
  a->nodetype = 'K';
  a->number =d;
  return (struct ast*)a;
}
double eval(const struct ast*a){
  double v=0.0;
  switch(a->nodetype){
    case 'K': v =((struct numval*)a)->number; break;
    case '+': v = eval(a->left)+eval(a->right); break;
    case '-': v = eval(a->left)-eval(a->right); break;
    case '*': v = eval(a->left)*eval(a->right); break;
    case '/': v = eval(a->left)/eval(a->right); break;
    case 'M': v = -eval(a->left); break;
    default: printf("internal error : bad node %c \n",a->nodetype);
      
  }
  return v;
}
void treefree(struct ast *a){
  switch(a->nodetype){
    case '+':
    case '-':
    case '*':
    case '/': treefree(a->right); treefree(a->left); free(a); break;
    case '|':
    case 'M': treefree(a->left); free(a); break;
    case 'K': free(a); break;
    default:
      printf("internal error : free bad node %c\n",a->nodetype);
      
  }
}
void yyerror(const char* s,...){
  va_list ap;
  va_start(ap ,s);
  fprintf(stderr,"%d: error",yylineno);
  vfprintf(stderr,s,ap);
  fprintf(stderr,"\n");
}
int main(){
  printf(">");
  return yyparse();
}
